const express = require('express');
const app = express();
const RSI = require('technicalindicators').RSI;
const MACD = require('technicalindicators').MACD;
const expressWs = require('express-ws')(app);
const binance = require('node-binance-api');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
const log_stdout = process.stdout;

console.log = function(d) {
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};
// binance.options({
// 'APIKEY':'GadBm83zN8GglCDuTA7uHaU9egTG9FkGovH8s6SRRdSCBKFjB6NY7njrrznVmQuN',
// 'APISECRET':'slXy3eS2IfYtOpLBH5D3S41UUsG4iBOVKsve67B1v11wMLo7qhDAk4OMmLroeXEi',
// recvWindow: 60000,
// test: true
// });

binance.options({
    'APIKEY':'qbPxbHKedzioUhHi2fnyTO7lsuEvI3ugqdSupAd3CkbTJ1AF4cePxZFmrc4Za8md',
    'APISECRET': 'NTgszRsXscjAtyjaEuDegpGhr2tekIWQd39nY1xsb8VSswTofoH33Bz7AZp1gN8r',
    recvWindow: 60000,
    test: true
});

/*OPTIONS*/
const lists = {};

const coins = ['ADABTC', 'TNTBTC', 'SNTBTC', 'XLMBTC', 'XVGBTC'];
const transactionHistory = [];
let socket = {};
const transactions = {
    buy: {},
    sell: {}
};
let isWalletUpdating = false;
const wallet = {};

// The only time the user data (account balances) and order execution websockets will fire, is if you create or cancel an order, or an order gets filled or partially filled
function balance_update(data) {
    console.log("Balance Update");
    for ( let obj of data.B ) {
        let { a:asset, f:available, l:onOrder } = obj;
        if ( available == "0.00000000" ) continue;
        console.log(asset+"\tavailable: "+available+" ("+onOrder+" on order)");
    }
}
function execution_update(data) {
    let { x:executionType, s:symbol, p:price, q:quantity, S:side, o:orderType, i:orderId, X:orderStatus } = data;
    if ( executionType == "NEW" ) {
        if ( orderStatus == "REJECTED" ) {
            console.log("Order Failed! Reason: "+data.r);
        }
        console.log(symbol+" "+side+" "+orderType+" ORDER #"+orderId+" ("+orderStatus+")");
        console.log("..price: "+price+", quantity: "+quantity);
        return;
    }
    //NEW, CANCELED, REPLACED, REJECTED, TRADE, EXPIRED
    console.log(symbol+"\t"+side+" "+executionType+" "+orderType+" ORDER #"+orderId);
}

/**
 * Write message object to the logs
 * @param logMessage
 */
function log(logMessage) {
    console.log(JSON.stringify(logMessage));
}

function calculateRSI(list) {
    if (list.length > 14) {
        return RSI.calculate({values: list.map((item) => item.close), period: 14}).map((value, index) => ({
            value: value,
            time: list[index + 12].time
        }));
    }
    return [];
}


function calculateMACD(list) {
    if (list.length > 14) {
        return MACD.calculate({
            values: list.map((item) => item.close),
            fastPeriod: 12,
            slowPeriod: 26,
            signalPeriod: 9
        });
    }
    return [];
}

/**
 * Getting RSI status buy last value in list of values
 * @param list
 * @param min
 * @param max
 * @param errorValue
 * @returns {*}
 */
function rsiStatus(list, min, max, errorValue) {
    if (list) {
        const lastValue = list[list.length - 1];

        if (lastValue >= max) {
            return {message: 'Need to sell', value: lastValue, max, status: 'SELL'}
        } else if (lastValue <= min) {
            return {message: 'Need to buy', value: lastValue, min, status: 'BUY'}
        } else if (lastValue + errorValue >= max) {
            return {message: 'One may to sell', value: lastValue, max, status: 'SELL_OK'}
        } else if (lastValue - errorValue <= min) {
            return {message: 'One may to buy', value: lastValue, min, status: 'BUY_OK'}
        } else {
            return {message: 'Average', value: lastValue, max, min, status: 'Average'}
        }
    }
}

function getMACDStatus(list) {
    if (list && list.length) {
        const last = list[list.length - 1];

        if (last && last.histogram) {
            return {state: last.histogram  > 0, status: last.histogram  > 0 ? 'BUY' : 'SELL'}
        }
    }

    return null;
}

function findLastSymbolTransaction(symbol) {
    const index = transactionHistory.map((item) => item.symbol).lastIndexOf(symbol);

    if (index !== -1) {
        return transactionHistory[index];
    }

    return null;
}

function send(messageObject) {
    socket.send(JSON.stringify(messageObject));
}

function transactionListUpdated() {
    send(transactionHistory);
}

function updateWallet() {
    isWalletUpdating = true;
    binance.balance((error, balances) => {
        wallet.BTC = balances.BTC;

        coins.filter((coin) => balances[coin.replace('BTC', '')])
            .forEach((coin) => wallet[coin] = balances[coin.replace('BTC', '')]);

        isWalletUpdating = false;

        const walletMessage = {message: 'Wallet updated', wallet: Object.keys(wallet).map((key) => ({[key]: +wallet[key].available}))};

        // send(walletMessage);
        log(walletMessage)
    });
}

/**
 * Method to estimate total fee of trancation
 * @param price
 * @param amount
 * @param feePercent
 * @returns {number}
 */
function estimateTotalFee(price, amount, feePercent = 0.001) {
    return price * amount * feePercent;
}

function sellCoin(symbol, price) {

    const lastBuyTransaction = findLastSymbolTransaction(symbol);

    if (lastBuyTransaction) {
        const fee = estimateTotalFee(lastBuyTransaction.price, lastBuyTransaction.amount);

        log({message: 'Sell options: ', symbol, buyPrice: lastBuyTransaction.price, sellPrice: price, amount: lastBuyTransaction.amount, fee, profitFee: (fee / lastBuyTransaction.amount) })

        if ((price - (fee / lastBuyTransaction.amount)) <= lastBuyTransaction.price) {
            const errorMessage = {message: `Can't sell because need to get profit`, symbol, buyPrice: lastBuyTransaction.price, sellPrice: price, amount: lastBuyTransaction.amount, fee};

            log(errorMessage);
            return;
        }
    } else {
        console.log('No last bu transaction for ' + symbol);
        return;
    }

    const walletAvailableSum = +wallet[symbol].available;
    const amount = +Math.floor(walletAvailableSum) || lastBuyTransaction.amount;

    send({message: 'Sell', amount, buyPrice: lastBuyTransaction.price, sellPrice: price, symbol, fee: estimateTotalFee(lastBuyTransaction.price, lastBuyTransaction.amount)});
    transactionHistory.push({
        type: 'SELL', price, symbol
    });

    if (!transactions.sell[symbol] && !isWalletUpdating && amount > 0) {

        send({message: 'sell', amount: amount, symbol});
        transactions.sell[symbol] = true;
        binance.sell(symbol, amount, price, {type:'LIMIT'}, (error, sellResponse) => {
            if (error) {
                const resultMessage = {message: 'Sell error!', error};
                console.log(JSON.stringify(resultMessage));
                send(resultMessage);
                return;
            }
            console.log(JSON.stringify({message: 'Sell completed: ', sellResponse}));
            send({
                message: 'Sell completed.',
                date: new Date().toISOString(),
                response: {message: sellResponse}
            });
            updateWallet();
            transactions.sell[symbol] = false;
        });

        transactionListUpdated();
    }
}

function buyCoin(symbol, price) {
    const symbolAvailable = +wallet[symbol].available;
    const symbolOnOrder = +wallet[symbol].onOrder;

    if (!transactions.buy[symbol] && !isWalletUpdating && symbolOnOrder === 0 && symbolAvailable < 1) {

        const amount = +Math.ceil(0.002 / price);
        const buyMessage = {message: 'Buy ordered', amount, symbol, price};

        send(buyMessage);
        log(buyMessage);

        transactions.buy[symbol] = true;
        binance.buy(symbol, amount, price, {type: 'LIMIT'}, (error, response) => {
            if (error) {
                const resultMessage = {message: 'Buy error!', error};
                console.log(JSON.stringify(resultMessage));
                return;
            }
            transactionHistory.push({type: 'BUY', price, symbol, amount});
            console.log(JSON.stringify({message: 'Buy response: ', response}));

            if (response) {
                const resultMessage = {
                    message: 'Buy completed.',
                    quantity: response.executedQty,
                    symbol: response.symbol,
                    status: response.status,
                    id: response.orderId
                };

                send(resultMessage);
                console.log(JSON.stringify(resultMessage))
            }
            transactions.buy[symbol] = false;

            transactionListUpdated();
        });
    }
}

function handleStatus(rsiStatus, macdStatus, lastPrice, symbol) {
    log({rsiStatus, macdStatus, symbol})

    const lastTransaction = findLastSymbolTransaction(symbol);

    if (rsiStatus === macdStatus) {
        log({message: 'RESULT SAME', symbol, lastPrice, rsi: rsiStatus, macd: macdStatus});

        if (rsiStatus === 'BUY') {
            if (lastTransaction && lastTransaction.type === 'SELL' && lastTransaction.symbol === symbol) {
                buyCoin(symbol, lastPrice);
            } else if (!lastTransaction) {
                buyCoin(symbol, lastPrice);
            }
        } else if (rsiStatus === 'SELL') {
            if (lastTransaction && lastTransaction.type === 'BUY' && lastTransaction.symbol === symbol) {
                sellCoin(symbol, lastPrice);
            }
        }
    }

    if (macdStatus === 'BUY' && rsiStatus === 'BUY_OK') {

        if (lastTransaction && lastTransaction.type === 'SELL' && lastTransaction.symbol === symbol) {
            buyCoin(symbol, lastPrice);
        } else if (!lastTransaction) {
            buyCoin(symbol, lastPrice);
        }
    } else if (macdStatus === 'SELL' || rsiStatus === 'SELL_OK') {

        if (lastTransaction && lastTransaction.type === 'BUY' && lastTransaction.symbol === symbol) {
            sellCoin(symbol, lastPrice);
        }
    }
}

const orders = {};

function compare(a,b) {
    if (+a.p > +b.p)
        return -1;
    if (+a.p < +b.p)
        return 1;
    return 0;
}

function addOrder(symbol, price, quantity, maker) {
    const order = {
        s: symbol,
        p: price,
        q: quantity
    };

    let list = [];
    if (!maker) {
        orders[symbol].buys.push(order);
        list = orders[symbol].buys;
    } else {
        orders[symbol].sells.push(order);
        list = orders[symbol].sells;
    }

    list = list.sort(compare);
    if (list.length > 10) {
        list.splice(list.length - 1, 1);
    }
}

function startWatchingForOrders() {
    binance.websockets.trades(coins, (trades) => {
        let {e:eventType, E:eventTime, s:symbol, p:price, q:quantity, m:maker, a:tradeId} = trades;

        addOrder(symbol, price, quantity, maker);
    });
}

app.ws('/orders', (ws, req) => {
    coins.forEach((key) => {

       ws.send(JSON.stringify(orders[key].buys));
       ws.send(JSON.stringify(orders[key].sell));
    });
});

app.ws('/check', (ws, req) => {
    console.log('WS connected');
    socket = ws;

    updateWallet();

    /*Start watching for user updates*/
    binance.websockets.userData(balance_update, execution_update);

    /*Initializing rsi array coins*/
    coins.forEach((key) => {
        lists[key] = {rsi: [], macd: []};
        orders[key] = {
            buys: [],
            sells: []
        }
    });

    binance.websockets.candlesticks(coins, '1m', (candlesticks) => {
        let {e: eventType, E: eventTime, s: symbol, k: ticks} = candlesticks;
        let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;

        const lastPrice = {
            time: Date.now(),
            close: +close
        };

        lists[symbol].rsi.push(lastPrice);
        lists[symbol].macd.push(lastPrice);
        const rsi = calculateRSI(lists[symbol].rsi);
        const macd = calculateMACD(lists[symbol].macd);

        if(macd.length && rsi.length) {
            const macdStatus = getMACDStatus(macd);
            const rsiStatusRes = rsiStatus(rsi.map((item) => item.value), 30, 70, 1.5);

            if (rsiStatusRes && macdStatus) {
                handleStatus(rsiStatusRes.status, macdStatus.status, +close, symbol);
            }
        }

    });

    coins.forEach((key) => {
        binance.aggTrades(key, {}, (error, trades) => {
            if (!error && trades) {

                const buys = [];
                const sells = [];

                trades.forEach((trade) => {
                    if (trade.m) {
                        sells.push(trade);
                    } else {
                     buys.push(trade);
                    }
                });
                orders[key].buys = buys.sort(compare).slice(0, 10).map((trade) => ({s: key, p: trade.p, q: trade.q}));
                orders[key].sells = sells.sort(compare).slice(0, 10).map((trade) => ({s: key, p: trade.p, q: trade.q}));

                startWatchingForOrders();
            }
        });
    });

    function findBig(values) {
        const list = Object.keys(values)
            .map((price) => ({p: price, q: values[price]}));
        log(list);

        return list.splice(0, 10).sort((a, b) => b.q - a.q);
    }

    binance.websockets.depthCache(['ENGBTC'], (symbol, depth) => {
        let bids = binance.sortBids(depth.bids);
        let asks = binance.sortAsks(depth.asks);

        send({m: 'res', symbol, bid: binance.first(bids), ask: binance.first(asks)})
    });


    // coins.forEach((symbol) => {
    //     binance.websockets.prevDay(symbol, (error, response) => {
    //         if (error) {
    //             log(error);
    //             send({message: 'Error tick', symbol});
    //             return;
    //         }
    //
    //         const lastPrice = {
    //             time: Date.now(),
    //             close: +response.close
    //         };
    //
    //         lists[symbol].rsi.push(lastPrice);
    //         lists[symbol].macd.push(lastPrice);
    //         const rsi = calculateRSI(lists[symbol].rsi);
    //         const macd = calculateMACD(lists[symbol].macd);
    //
    //         if(macd.length && rsi.length) {
    //             const macdStatus = getMACDStatus(macd);
    //             const rsiStatusRes = rsiStatus(rsi.map((item) => item.value), 30, 70, 1.5);
    //
    //             if (rsiStatusRes && macdStatus) {
    //                 handleStatus(rsiStatusRes.status, macdStatus.status, response.close, symbol);
    //             }
    //         }
    //     });
    // });

    // binance.websockets.candlesticks(coins, '1m', (candlesticks) => {
    //     let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
    //     let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;
    //
    //     /*Creating new price object*/
    //     const lastPrice = {
    //         time: Date.now(),
    //         close: close
    //     };
    //
    //     lists[symbol].rsi.push(lastPrice);
    //     const rsi = calculateRSI(lists[symbol].rsi);
    //
    //     if (rsi && rsi.length > 0) {
    //         const rsiStatusRes = rsiStatus(rsi.map((item) => item.value), 30, 70, 1.5);
    //         log({...rsiStatusRes, ...lastPrice, symbol});
    //         handleStatus(rsiStatusRes.status, close, symbol);
    //     }
    // });
});

app.get('/sell', (req, res) => {
    binance.marketSell("XVGBTC", 254,(error, response) => {
        res.send(response);
    });
});

app.listen(3002, function () {
    console.log('Example app listening on port 3000!');
});