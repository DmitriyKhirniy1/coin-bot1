const express = require('express');
const app = express();
const binance = require('node-binance-api');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
const log_stdout = process.stdout;

console.log = function(d) {
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};
binance.options({
    'APIKEY':'GadBm83zN8GglCDuTA7uHaU9egTG9FkGovH8s6SRRdSCBKFjB6NY7njrrznVmQuN',
    'APISECRET':'slXy3eS2IfYtOpLBH5D3S41UUsG4iBOVKsve67B1v11wMLo7qhDAk4OMmLroeXEi',
    recvWindow: 60000,
    test: true
});
const tickSteps = {};
const symbols = {};
const markets = [];

const expressWs = require('express-ws')(app);
let indexes = {};
let tickSymbol;
let tickNum = 0;
const transactions = {
    buy: {},
    sell: {}
};

const buyTransaction = {};
const wallet = {};
let exchange = {};
let socket = null;

/*Setting tick step by current our market prices*/
function setTickSteps(prices) {
    markets.forEach((market) => {
        const title = market.market;
        const marketTicks = getTicks(+prices[title], symbols[title].step, 200);
        tickSteps[title] = marketTicks;
        symbols[title].priceMin = marketTicks[100].min;
        symbols[title].priceMax = marketTicks[100].max;
    });
}

function send(message) {
    if (socket) {
        socket.send(JSON.stringify(message));
    }
}

function setCoin({coinName, priceMin, priceMax, step, lastBuyPrice}) {
    const coin = coinName + 'BTC';

    symbols[coin] = {
        priceMin: priceMin || 0,
        priceMax: priceMax || 0,
        step: step || 0.00000020
    };
    tickSymbol = coin;
    markets[0] = {
        coin: coinName,
        market: coin
    };
    transactions.buy[coin] = false;
    transactions.sell[coin] = false;
    indexes[coin] = 100;
    buyTransaction[coin] = lastBuyPrice || 0;
}

app.ws('/price', function(ws, req) {

    ws.on('message', (msg) => {
        const message = JSON.parse(msg);

        switch (message.type) {
            case 'SET_COIN': {
                setCoin(message.payload);
                binance.prices((error, ticker) => {
                    const prices = {};
                    prices.BTCUSDT = ticker.BTCUSDT;
                    markets.forEach(({market}) => prices[market] = ticker[market]);
                    send({type: 'COIN_PRICE', payload: prices});

                    binance.exchangeInfo((error, data) => {
                        exchange = parseExchangeInfo(data);
                        send({type: 'COIN_EXCHANGE_INFO', payload: exchange})
                    });

                    if (!exchange) {
                        send({message: 'Exchange info error.'});
                        return;
                    }

                    setTickSteps(prices);
                    startWatching(ws);
                });
            }
            break;
            case 'GET_BALANCE': {
                binance.balance((error, balances) => {
                    const balance = {};
                    markets
                        .filter((market) => balances[market.coin])
                        .forEach((market) => balance[market.market] = balances[market.coin]);

                    send({type: 'SET_BALANCE', payload: balance});
                });
            }
        }
    });

    console.log('WS connected');
    socket = ws;
});

let isWalletUpdating = false;
function updateWallet() {
    isWalletUpdating = true;
    binance.balance((error, balances) => {
        wallet.BTC = balances.BTC;

        markets
            .filter((market) => balances[market.coin])
            .forEach((market) => wallet[market.market] = balances[market.coin]);
        isWalletUpdating = false;
    });
}

function startWatching(ws) {
    updateWallet();
    binance.websockets.prevDay(tickSymbol, (response) => {
        let ordered = false;
        let market = markets[0];

        send({type: 'TICKER', payload: response});

        const lastPrice = +response.close;

        if (!isWalletUpdating) {

            const walletAvailableSum = +wallet[market.market].available;
            let buySumAvailable = +wallet.BTC.available;

            console.log(`check buy: lastPrice: ${lastPrice}, priceToBuy: ${symbols[market.market].priceMin} sumToBuyAvailable: ${buySumAvailable}`);


            if (lastPrice <= symbols[market.market].priceMin && buySumAvailable) {

                /**
                 * Whether we don't have pending buy transactions with this coin
                 */
                if (!transactions.buy[market.market]) {
                    const amount = +Math.floor(buySumAvailable / lastPrice);

                    /*Current coin exchange info*/
                    const coinExchange = exchange[market.market];

                    console.log(JSON.stringify({message: 'Amount: ',market: market.market, amount, coinExchange}));

                    if (coinExchange && amount >= +coinExchange.minQty && buySumAvailable >= +coinExchange.minNotional) {
                        transactions.buy[market.market] = true;
                        buyTransaction[market.market] = lastPrice;

                        binance.buy(market.market, amount, lastPrice, {}, (error, buyRes) => {
                            if (error) {
                                const resultMessage = {type: 'BUY_ERROR', payload: error};
                                console.log(JSON.stringify(resultMessage));
                                ws.send(JSON.stringify(resultMessage));
                                return;
                            }
                            console.log(JSON.stringify({message: 'Buy completed: ', payload: buyRes}));
                            send(JSON.stringify({type: 'BUY_COMPLETED.', payload: buyRes}));
                            buyTransaction[market.market] = 0;
                            updateWallet();
                            transactions.buy[market.market] = false;
                        });

                        console.log(JSON.stringify({message: `Buy ordered: ${market.market}`, amount: amount, price: lastPrice}));

                        send({type: 'BUY_ORDERED',
                            payload: {
                                symbol: market.market,
                                origQty: amount,
                                price: lastPrice,
                                time: new Date(),
                                side: 'BUY'
                        }});
                    } else {
                        console.log({status: 'ERROR', message: `Can't buy ${market.market} because of max coin buy limit of reached: ${walletAvailableSum}`});
                    }
                } else {
                    console.log('CANT BUY, ORDER IS PROCESSING');
                }
            }

            /*Sell logic*/

            console.log(JSON.stringify({message: 'check sell: ', lastPrice, priceMin: symbols[market.market].priceMax, walletAvailableSum, sellOrderProcessing: transactions.sell[market.market]}));
            if (lastPrice >= symbols[market.market].priceMax && walletAvailableSum) {
                if (!transactions.sell[market.market]) {
                    const amount = +Math.floor(walletAvailableSum);
                    const exchangeInfo = exchange[market.market];
                    if (amount >= exchangeInfo.minQty) {
                        console.log(JSON.stringify({message: 'wang sell: ',
                            amount,
                            market: market.market,
                            lastPrice,
                            sellIsOrdering: buyTransaction[market.market],
                            ok: lastPrice >= buyTransaction[market.market]}));

                        if (lastPrice >= buyTransaction[market.market] + (symbols[market.market].step / 2)) {
                            transactions.sell[market.market] = true;
                            /*Sell coin*/

                            binance.sell(market.market, amount, lastPrice, {}, (error, sellResponse) => {
                                if (error) {
                                    const resultMessage = {type: 'SELL_ERROR', payload: error};
                                    console.log(JSON.stringify(resultMessage));
                                    ws.send(JSON.stringify(resultMessage));
                                    return;
                                }
                                console.log(JSON.stringify({message: 'Sell completed: ', sellResponse}));
                                send({type: 'SELL_COMPLETED', payload: sellResponse});

                                /*Reset coin buy price*/
                                buyTransaction[market.market] = 0;
                                transactions.sell[market.market] = false;
                                updateWallet();
                            });

                            console.log(JSON.stringify({message: `Sell: ${market.market}`, amount: amount, price: lastPrice}));

                            send({type: 'SELL_ORDERED',
                                payload: {
                                    symbol: market.market,
                                    origQty: amount,
                                    price: lastPrice,
                                    time: new Date(),
                                    side: 'SELL'
                                }});
                        } else {
                            console.log(`wont sell , buy for ${buyTransaction[market.market]} , try to sell for ${lastPrice}`);
                        }
                    }
                } else {
                    console.log('CANT SELL, ORDER IS PROCESSING');
                }
            }
            if (tickNum % 10 === 0) {
                const marketIndex = indexes[market.market];
                const marketSteps = tickSteps[market.market];
                const stepRes = getSteps(marketIndex, lastPrice, marketSteps);
                if (stepRes.changed) {
                    indexes[market.market] = stepRes.index;
                    symbols[market.market] = {
                        priceMin: stepRes.step.min,
                        priceMax: stepRes.step.max
                    };
                    const message = {
                        type: 'NEW_STEP',
                        payload: {
                            step: stepRes.step,
                            lastPrice: lastPrice,
                            symbol: market.market,
                            index: stepRes.index,
                            date: new Date()
                        }
                    };
                    console.log(JSON.stringify(message));
                    ws.send(JSON.stringify(message));
                }
            }
        }

        tickNum++;
    });
}

app.listen(3001, function () {
    console.log('Example app listening on port 3000!');
});

function getTicks(value, step, valuesCount) {
    const ticks = new Array(valuesCount);
    const median = ~~(valuesCount / 2);
    ticks[median] = {
        min: +(value).toFixed(9),
        max: +(value + step).toFixed(9)
    };
    let currentValue = value;
    for (let i = median - 1;i >= 0; i--) {
        let value = currentValue;
        ticks[i] = {
            min: +(value - step).toFixed(9),
            max: +(value).toFixed(9)
        };
        currentValue-=  step;
    }
    currentValue = +(value).toFixed(9);
    for (let i = median + 1;i <= valuesCount; i++) {
        value = +(currentValue + step).toFixed(9);
        ticks[i] = {
            min: value,
            max: +(value + step).toFixed(9)
        };
        currentValue+= +(step).toFixed(9);
    }
    return ticks;
}
function getSteps(currentIndex, currentValue, steps) {
    const currentStep = steps[currentIndex] || steps[steps.length - 1];
    let index = currentIndex;
    let changed = false;
    if (currentValue < currentStep.min) {
        let innerIndex = index;
        while (true) {
            const step = steps[innerIndex];
            if (step && currentValue > step.min && currentValue <= step.max) {
                index = innerIndex;
                break;
            }
            if (!step) {
                index = ~~(steps.length / 2);
                break;
            }
            innerIndex--;
        }
        changed = true;
    } else if (currentValue >= currentStep.max) {
        let innerIndex = index;
        while (true) {
            const step = steps[innerIndex];
            if (step && currentValue >= step.min && currentValue < step.max) {
                index = innerIndex;
                break;
            }
            if (!step) {
                index = steps.length - 1;
                break;
            }
            innerIndex++;
        }
        changed = true;
    }
    return {
        index,
        changed,
        step: steps[index]
    }
}

function parseExchangeInfo(data) {
    let minimums = {};
    for ( let obj of data.symbols ) {
        if (markets.some((market) => market.market === obj.symbol)) {
            let filters = {minNotional:0.001,minQty:1,maxQty:10000000,stepSize:1,minPrice:0.00000001,maxPrice:100000};
            for ( let filter of obj.filters ) {
                const type = filter.filterType;

                if ( type === "MIN_NOTIONAL" ) {
                    filters.minNotional = filter.minNotional;
                } else if ( type === "PRICE_FILTER" ) {
                    filters.minPrice = filter.minPrice;
                    filters.maxPrice = filter.maxPrice;
                } else if ( type === "LOT_SIZE" ) {
                    filters.minQty = filter.minQty;
                    filters.maxQty = filter.maxQty;
                    filters.stepSize = filter.stepSize;
                }
            }
            minimums[obj.symbol] = filters;
        }
    }
    return minimums;
}

function trunc(value, digits = 8) {
    return +(value).toFixed(digits);
}