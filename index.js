const express = require('express');
const app = express();
const binance = require('node-binance-api');
const fs = require('fs');
const util = require('util');
const log_file = fs.createWriteStream(__dirname + '/debug.log', {flags : 'w'});
const log_stdout = process.stdout;

console.log = function(d) {
    log_file.write(util.format(d) + '\n');
    log_stdout.write(util.format(d) + '\n');
};
binance.options({
    'APIKEY':'GadBm83zN8GglCDuTA7uHaU9egTG9FkGovH8s6SRRdSCBKFjB6NY7njrrznVmQuN',
    'APISECRET':'slXy3eS2IfYtOpLBH5D3S41UUsG4iBOVKsve67B1v11wMLo7qhDAk4OMmLroeXEi',
    recvWindow: 60000
});
const tickSteps = {};
const symbols = {
    'SNTBTC': {
        priceMin: 0,
        priceMax: 0,
        step: 0.00000020
    }
};
const markets = [
    {
        coin: 'SNT',
        market: 'SNTBTC'
    }
];
const coinBalancMarkets = markets.map((market) => market.coin);
const expressWs = require('express-ws')(app);
app.get('/', function (req, res) {
    res.send('Hello World!');
});
let indexes = {
    'SNTBTC': 100,
};
let tickNum = 0;
const transactions = {
    buy: {
        'SNTBTC': false,
    },
    sell: {
        'SNTBTC': false,
    }
};
const buyTransaction = {
    'SNTBTC': 0.00002866
};
const wallet = {};
let exchange = {};
let socket = null;

/*Setting tick step by current our market prices*/
function setTickSteps(prices) {
    markets.forEach((market) => {
        const title = market.market;
    const marketTicks = getTicks(+prices[title], symbols[title].step, 200);
    tickSteps[title] = marketTicks;
    symbols[title].priceMin = marketTicks[100].min;
    symbols[title].priceMax = marketTicks[100].max;
});
}


function send(message) {
    if (socket) {
        socket.send(JSON.stringify(message));
    }
}

app.ws('/price', function(ws, req) {
    console.log('WS connected');
    socket = ws;
    binance.prices((error, ticker) => {
        const prices = {};
        markets.forEach(({market}) => prices[market] = ticker[market]);
        send({message: 'Getting prices.', prices});

        binance.exchangeInfo((error, data) => {
            exchange = parseExchangeInfo(data);
            send({message: 'Exchange info.', exchange})
        });

        setTickSteps(prices);
        startWatching(ws);
    });
});

function trunc(value, digits = 8) {
    return +(value).toFixed(digits);
}

function logValues(prices) {
    console.log(JSON.stringify({message: 'Prices: ', prices}));
    console.log('Symbols: ', symbols);
}

let isWalletUpdating = false;
function updateWallet() {
    isWalletUpdating = true;
    binance.balance((error, balances) => {
        wallet.BTC = balances.BTC;

        markets
            .filter((market) => balances[market.coin])
            .forEach((market) => wallet[market.market] = balances[market.coin]);
        isWalletUpdating = false;
    });
}

function startWatching(ws) {
    updateWallet();
    binance.websockets.prevDay('SNTBTC', (response) => {
        let ordered = false;
        let market = markets[0];
        const lastPrice = +response.close;

        if (!isWalletUpdating) {
            let buySumAvailable = +wallet.BTC.available;
            const walletAvailableSum = +wallet[market.market].available;
            console.log(`check buy: lastPrice: ${lastPrice}, priceToBuy: ${symbols[market.market].priceMin} sumToBuyAvailable: ${buySumAvailable}`);
            if (lastPrice <= symbols[market.market].priceMin && buySumAvailable) {
                if (!transactions.buy[market.market]) {
                    const amount = +Math.floor(buySumAvailable / lastPrice);
                    const coinExchange = exchange[market.market];

                    console.log(JSON.stringify({message: 'Amount: ',market: market.market, amount}));

                    if (amount >= +coinExchange.minQty && buySumAvailable >= +coinExchange.minNotional) {
                        transactions.buy[market.market] = true;
                        buyTransaction[market.market] = lastPrice;
                        binance.buy(market.market, amount, lastPrice, {}, (error, buyRes) => {
                            if (error) {
                                const resultMessage = {message: 'Buy error!', error};
                                console.log(JSON.stringify(resultMessage));
                                ws.send(JSON.stringify(resultMessage));
                                return;
                            }
                            console.log(JSON.stringify({message: 'Buy response: ', buyRes}));
                            if (buyRes && buyRes.orderId) {
                                const resultAmount = (buyRes.executedQty * buyTransaction[market.market]);
                                const resultMessage = {message: 'Buy completed.', quantity: buyRes.executedQty, symbol: buyRes.symbol, resultAmount};
                                ws.send(JSON.stringify(resultMessage));
                                console.log(JSON.stringify({message: 'Buy completed.', res: buyRes}));
                                updateWallet();
                            } else {
                                buyTransaction[market.market] = 0;
                            }
                            transactions.buy[market.market] = false;
                        });
                        ordered = true;
                        console.log(JSON.stringify({message: `Buy ordered: ${market.market}`, amount: amount, price: lastPrice}));
                        ws.send(JSON.stringify({
                            message: 'Buy ordered.',
                            market: market.market,
                            amount: amount,
                            price: lastPrice
                        }));
                    } else {
                        console.log({status: 'ERROR', message: `Can't buy ${market.market} because of max coin buy limit of reached: ${walletAvailableSum}`});
                    }
                } else {
                    console.log('CANT BUY, ORDER IS PROCESSING');
                }
            }
            console.log(JSON.stringify({message: 'check sell: ', lastPrice, priceMin: symbols[market.market].priceMax, walletAvailableSum, sellOrderProcessing: transactions.sell[market.market]}));
            if (lastPrice >= symbols[market.market].priceMax && walletAvailableSum) {
                if (!transactions.sell[market.market]) {
                    const amount = +Math.floor(walletAvailableSum);
                    if (amount >= 1) {
                        console.log(JSON.stringify({message: 'wang sell: ',
                            amount,
                            market: market.market,
                            lastPrice,
                            sellIsOrdering: buyTransaction[market.market],
                            ok: lastPrice >= buyTransaction[market.market]}));
                        if (lastPrice >= buyTransaction[market.market]) {
                            transactions.sell[market.market] = true;
                            /*Sell coin*/
                            binance.sell(market.market, amount, lastPrice, {}, (error, sellResponse) => {
                                if (error) {
                                    const resultMessage = {message: 'Sell error!', error};
                                    console.log(JSON.stringify(resultMessage));
                                    ws.send(JSON.stringify(resultMessage));
                                    return;
                                }
                                console.log(JSON.stringify({message: 'Sell completed: ', sellResponse}));
                                /*Reset coin buy price*/
                                buyTransaction[market.market] = 0;
                                ws.send(JSON.stringify({
                                    message: 'Sell completed.',
                                    date: new Date().toISOString(),
                                    response: {message: sellResponse}
                                }));
                                updateWallet();
                                transactions.sell[market.market] = false;
                            });
                            ordered = true;
                            console.log(JSON.stringify({message: `Sell: ${market.market}`, amount: amount, price: lastPrice}));
                            console.log(JSON.stringify({message: 'Wallet store: ', wallet: wallet[market.market]}));
                            ws.send(JSON.stringify({
                                message: 'Sell ordered',
                                market: market.market,
                                amount: amount,
                                price: lastPrice,
                                date: new Date().toISOString()
                            }));
                        } else {
                            console.log(`wont sell , buy for ${buyTransaction[market.market]} , try to sell for ${lastPrice}`);
                        }
                    }
                } else {
                    console.log('CANT SELL, ORDER IS PROCESSING');
                }
            }
            if (tickNum % 10 === 0) {
                const marketIndex = indexes[market.market];
                const marketSteps = tickSteps[market.market];
                const stepRes = getSteps(marketIndex, lastPrice, marketSteps);
                if (stepRes.changed) {
                    indexes[market.market] = stepRes.index;
                    symbols[market.market] = {
                        priceMin: stepRes.step.min,
                        priceMax: stepRes.step.max
                    };
                    const message = {
                        message: `New step index in : ${market.market} , index: ${stepRes.index}`,
                        step: stepRes.step,
                        lastOpenPrice: lastPrice
                    };
                    console.log(JSON.stringify(message));
                    ws.send(JSON.stringify(message));
                }
            }
        }

        tickNum++;
    });
}

app.get('/prices', (req, res) => {
    binance.prevDay("BTCUSD", (error, prevDay, symbol) => {
        return res.send({error, prevDay, symbol})
    });
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});

function getTicks(value, step, valuesCount) {
    const ticks = new Array(valuesCount);
    const median = ~~(valuesCount / 2);
    ticks[median] = {
        min: +(value).toFixed(9),
        max: +(value + step).toFixed(9)
    };
    let currentValue = value;
    for (let i = median - 1;i >= 0; i--) {
        let value = currentValue;
        ticks[i] = {
            min: +(value - step).toFixed(9),
            max: +(value).toFixed(9)
        };
        currentValue-=  step;
    }
    currentValue = +(value).toFixed(9);
    for (let i = median + 1;i <= valuesCount; i++) {
        value = +(currentValue + step).toFixed(9);
        ticks[i] = {
            min: value,
            max: +(value + step).toFixed(9)
        };
        currentValue+= +(step).toFixed(9);
    }
    return ticks;
}
function getSteps(currentIndex, currentValue, steps) {
    const currentStep = steps[currentIndex] || steps[steps.length - 1];
    let index = currentIndex;
    let changed = false;
    if (currentValue < currentStep.min) {
        let innerIndex = index;
        while (true) {
            const step = steps[innerIndex];
            if (step && currentValue > step.min && currentValue <= step.max) {
                index = innerIndex;
                break;
            }
            if (!step) {
                index = ~~(steps.length / 2);
                break;
            }
            innerIndex--;
        }
        changed = true;
    } else if (currentValue >= currentStep.max) {
        let innerIndex = index;
        while (true) {
            const step = steps[innerIndex];
            if (step && currentValue >= step.min && currentValue < step.max) {
                index = innerIndex;
                break;
            }
            if (!step) {
                index = steps.length - 1;
                break;
            }
            innerIndex++;
        }
        changed = true;
    }
    return {
        index,
        changed,
        step: steps[index]
    }
}


function parseExchangeInfo(data) {
    let minimums = {};
    for ( let obj of data.symbols ) {
        if (markets.some((market) => market.market === obj.symbol)) {
            let filters = {minNotional:0.001,minQty:1,maxQty:10000000,stepSize:1,minPrice:0.00000001,maxPrice:100000};
            for ( let filter of obj.filters ) {
                const type = filter.filterType;

                if ( type === "MIN_NOTIONAL" ) {
                    filters.minNotional = filter.minNotional;
                } else if ( type === "PRICE_FILTER" ) {
                    filters.minPrice = filter.minPrice;
                    filters.maxPrice = filter.maxPrice;
                } else if ( type === "LOT_SIZE" ) {
                    filters.minQty = filter.minQty;
                    filters.maxQty = filter.maxQty;
                    filters.stepSize = filter.stepSize;
                }
            }
            minimums[obj.symbol] = filters;
        }
    }
    return minimums;
}