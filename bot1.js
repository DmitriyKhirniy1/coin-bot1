const fs = require('fs');
const obj = JSON.parse(fs.readFileSync('xlm-btc.json', 'utf8'));
const data = obj.Data;
const cors = require('cors');
console.log('data: ', data)

const getRsi = (rs) => 100 - (100 / (1 + rs));

const avg = (elmt, field) => {
    var sum = 0;
    let values = elmt.length;
    for( var i = 0; i < elmt.length; i++ ){
        if (elmt[i][field]) {
            sum += parseInt(elmt[i][field], 10); //don't forget to add the base
        } else {
            values--;
        }
    }

    return sum/values;
}

const average = (elements) => {
    let sum = 0;

    for(let i = 0; i < elements.length; i++ ){
        sum += elements[i] || 0;
    }
    console.log('length:', elements.length)

    return sum / elements.length;
};

// const closeData = data.map((item) => ({close: item.close}));
const closeData = data;

closeData.forEach((item , index) => {
    item.index = index + 1;
    if(index > 0) {
        const change = item.close - closeData[index - 1].close;
        item.change = change;

        if (change >= 0) {
            item.gain =  change;
        } else {
            item.lose = Math.abs(change);
        }
    }

    if (index === 13){
        item.avgGain = average(closeData.slice(0, index + 1).map((item) => item.gain));
        item.avgLose = average(closeData.slice(0, index + 1).map((item) => item.lose));
    } else if (index > 13) {

        item.avgGain = (closeData[index - 1].avgGain * (index - 1) + (item.gain || 0)) / (index + 1);
        item.avgLose = (closeData[index - 1].avgLose * (index - 1) + (item.lose || 0)) / (index + 1);
        item.rs = item.avgGain / item.avgLose;
        item.rsi = getRsi(item.rs);
    }
});

const express = require('express');
const app = express();
app.use(cors());

app.get('/rsi', (req, res) => {
    return res.send(closeData);
});


app.listen(3002, function () {
    console.log('Example app listening on port 3000!');
});