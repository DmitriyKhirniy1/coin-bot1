const fs = require('fs');
const obj = JSON.parse(fs.readFileSync('test-data.json', 'utf8'));
const data = obj.Data;
const { Layer, Network } = require('synaptic');

var inputLayer = new Layer(3);
var hiddenLayer = new Layer(4);
var outputLayer = new Layer(1);

const trainingData = data.map((item) => ({close: item.close, high: item.high, low: item.low, open: item.open}));

const normalize = (current, min, max) => (current - min) / (max -min);
const denormalize = (current, min, max) => current * (max - min) + min;

function randomInteger(min, max) {
    var rand = min - 0.5 + Math.random() * (max - min + 1)
    rand = Math.round(rand);
    return rand;
}

function softmax(arr) {
    return arr.map(function(value,index) {
        return Math.exp(value) / arr.map( function(y /*value*/){ return Math.exp(y) } ).reduce( function(a,b){ return a+b })
    })
}

const closeMax = Math.max(...trainingData.map((item) => item.close));
const closeMin = Math.min(...trainingData.map((item) => item.close));
const highMax = Math.max(...trainingData.map((item) => item.close));
const highMin = Math.max(...trainingData.map((item) => item.close));
const lowMax = Math.max(...trainingData.map((item) => item.close));
const lowMin = Math.max(...trainingData.map((item) => item.close));
const openMax = Math.max(...trainingData.map((item) => item.close));
const openMIn = Math.max(...trainingData.map((item) => item.close));

function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

const fields = ['close', 'high', 'low', 'open'];
const mins = {};
const maxs = {}
fields.forEach((type) => {
    mins[type] = Math.min(...trainingData.map((item) => item[type]));
    maxs[type] = Math.max(...trainingData.map((item) => item[type]));
});
trainingData.forEach((item, index) => {
    fields.forEach((type) => {
        const value = normalize(item[type], mins[type], maxs[type]);
        item[type] = value;
    });
});


inputLayer.project(hiddenLayer);
hiddenLayer.project(outputLayer);
const myNetwork = new Network({
    input: inputLayer,
    hidden: [hiddenLayer],
    output: outputLayer
});

const learningRate = .3;

for (let i = 0;i<1000000;i++) {
    trainingData.forEach((item) => {
        myNetwork.activate([item.close, item.high, item.low]);
        myNetwork.propagate(learningRate, [item.open]);
    });
    shuffle(trainingData);
}

const index = 100;
// console.log(mins);
// console.log(maxs);
// console.log('data: ', data[index], trainingData[index]);
console.log(myNetwork.activate([trainingData[index].close, trainingData[index].high, trainingData[index].low]));

for (let i =0;i< 10;i++){
    const index =  randomInteger(0, trainingData.length);
    console.log('data: ', index, data[index]);
    console.log('train: ', trainingData[index]);
    const result = myNetwork.activate([trainingData[index].close, trainingData[index].high, trainingData[index].low]);
    console.log('result: ', result, denormalize(result, mins.open, maxs.open), data[index].open);
    console.log('----------------------------------')
}