const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
app.use(cors());
app.use(bodyParser.json());

const RSI = require('technicalindicators').RSI;
const MACD = require('technicalindicators').MACD;
const expressWs = require('express-ws')(app);
const binance = require('node-binance-api');
binance.options({
    'APIKEY':'GadBm83zN8GglCDuTA7uHaU9egTG9FkGovH8s6SRRdSCBKFjB6NY7njrrznVmQuN',
    'APISECRET':'slXy3eS2IfYtOpLBH5D3S41UUsG4iBOVKsve67B1v11wMLo7qhDAk4OMmLroeXEi',
    recvWindow: 60000,
    test: true
});
const lists = {};

const coins = ['XLMBTC', 'ADABTC', 'OSTBTC', 'AMBBTC', 'LINKBTC', 'YOYOBTC', 'WINGSBTC'];

function calculateRSI(list) {
    if (list.length > 14) {
        return RSI.calculate({values: list.map((item) => item.close), period: 14}).map((value, index) => ({
            value: value,
            time: list[index + 12].time
        }));
    }
    return [];
}

function calculateMACD(list) {
    return MACD.calculate({
        values: list,
        fastPeriod: 12,
        slowPeriod: 26,
        signalPeriod: 9
    });
}

function rsiStatus(list, min, max, errorValue) {
    if (list) {
        const lastValue = list[list.length - 1];

        if (lastValue >= max) {
            return {message: 'Need to sell', value: lastValue, max, status: 'SELL'}
        } else if (lastValue <= min) {
            return {message: 'Need to buy', value: lastValue, min, status: 'BUY'}
        } else if (lastValue + errorValue >= max) {
            return {message: 'One may to sell', value: lastValue, max, status: 'SELL_OK'}
        } else if (lastValue - errorValue <= min) {
            return {message: 'One may to buy', value: lastValue, min, status: 'BUY_OK'}
        } else {
            return {message: 'Average', value: lastValue, max, min}
        }
    }
}

const transactionHistory = [];
let socket = {};
const transactions = {
    buy: {},
    sell: {}
};
let isWalletUpdating = false;
const wallet = {};

function findLastSymbolTransaction(symbol) {
    const index = transactionHistory.map((item) => item.symbol).lastIndexOf(symbol);

    if (index !== -1) {
        return transactionHistory[index];
    }

    return null;
}

function send(messageObject) {
    socket.send(JSON.stringify(messageObject));
}

function transactionListUpdated() {
    send(transactionHistory);
}

function updateWallet() {
    isWalletUpdating = true;
    binance.balance((error, balances) => {
        wallet.BTC = balances.BTC;

        coins.filter((coin) => balances[coin.replace('BTC', '')])
            .forEach((coin) => wallet[coin] = balances[coin.replace('BTC', '')]);

        isWalletUpdating = false;
        send({message: 'Wallet updated', wallet: wallet})
    });
}

function sellCoin(symbol, price) {

    const walletAvailableSum = +wallet[symbol].available;
    const amount = +Math.floor(walletAvailableSum);

    if (!transactions.sell[symbol] && !isWalletUpdating && amount > 0) {

        transactionHistory.push({
            type: 'SELL', price, symbol
        });

        send({message: 'sell', amount: amount, symbol});
        transactions.sell[symbol] = true;
        binance.sell(symbol, amount, price, {type:'LIMIT'}, (error, sellResponse) => {
            if (error) {
                const resultMessage = {message: 'Sell error!', error};
                console.log(JSON.stringify(resultMessage));
                send(resultMessage);
                return;
            }
            console.log(JSON.stringify({message: 'Sell completed: ', sellResponse}));
            send({
                message: 'Sell completed.',
                date: new Date().toISOString(),
                response: {message: sellResponse}
            });
            updateWallet();
            transactions.sell[symbol] = false;
        });

        transactionListUpdated();
    }
}

function buyCoin(symbol, price) {
    const symbolAvailable = +wallet[symbol].available;
    const symbolOnOrder = +wallet[symbol].onOrder;

    if (!transactions.buy[symbol] && !isWalletUpdating && symbolOnOrder === 0 && symbolAvailable < 1) {

        transactionHistory.push({
            type: 'BUY', price, symbol
        });

        const amount = +Math.ceil(0.002 / price);
        send({message: 'Buy amount', amount, symbol, price});

        transactions.buy[symbol] = true;
        binance.buy(symbol, amount, price, {type: 'LIMIT'}, (error, response) => {
            if (error) {
                const resultMessage = {message: 'Buy error!', error};
                console.log(JSON.stringify(resultMessage));
                return;
            }
            console.log(JSON.stringify({message: 'Buy response: ', response}));

            if (response) {
                const resultMessage = {
                    message: 'Buy completed.',
                    quantity: response.executedQty,
                    symbol: response.symbol,
                    status: response.status,
                    id: response.orderId
                };

                send(resultMessage);
                console.log(JSON.stringify(resultMessage))
            }
            transactions.buy[symbol] = false;
        });

        transactionListUpdated();
    }
}

function handleStatus(rsiStatus, lastPrice, symbol) {
    const lastTransaction = findLastSymbolTransaction(symbol);

    if (rsiStatus === 'BUY' || rsiStatus === 'BUY_OK') {

        if (lastTransaction && lastTransaction.type === 'SELL' && lastTransaction.symbol === symbol) {
            buyCoin(symbol, lastPrice);
        } else if (!lastTransaction) {
            buyCoin(symbol, lastPrice);
        }
    } else if (rsiStatus === 'SELL' || rsiStatus === 'SELL_OK') {

        if (lastTransaction && lastTransaction.type === 'BUY' && lastTransaction.symbol === symbol) {
            sellCoin(symbol, lastPrice);
        }
    }
}

app.ws('/check', (ws, req) => {
    console.log('WS connected');
    socket = ws;

    updateWallet();

    coins.forEach((key) => lists[key] = {rsi: [], macd: []})

    binance.websockets.candlesticks(coins, '1m', (candlesticks) => {
        let { e:eventType, E:eventTime, s:symbol, k:ticks } = candlesticks;
        let { o:open, h:high, l:low, c:close, v:volume, n:trades, i:interval, x:isFinal, q:quoteVolume, V:buyVolume, Q:quoteBuyVolume } = ticks;

        /*Creating new price object*/
        const lastPrice = {
            time: Date.now(),
            close: close
        };

        lists[symbol].rsi.push(lastPrice);
        lists[symbol].macd.push(lastPrice);
        const rsi = calculateRSI(lists[symbol].rsi);
        const macd = calculateMACD(lists[symbol].macd);

        send({message: 'MACD', values: macd});
        console.log({message: 'MACD', values: macd});

        if (rsi && rsi.length > 0) {
            const rsiStatusRes = rsiStatus(rsi.map((item) => item.value), 30, 70, 1.5);
            handleStatus(rsiStatusRes.status, close, symbol);
        }
    });

});

app.listen(3002, function () {
    console.log('Example app listening on port 3000!');
});