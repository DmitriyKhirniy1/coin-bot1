const express = require('express');
const app = express();
const cors = require('cors');
const bodyParser = require('body-parser');
app.use(cors());
app.use(bodyParser.json());
const fs = require('fs');
const request = require('request');

const RSI = require('technicalindicators').RSI;
const MACD = require('technicalindicators').MACD;

const obj = JSON.parse(fs.readFileSync('data/xlm-btc.json', 'utf8'));
const data = obj.Data;
data.forEach((item) => item.time = +(item.time + '000'));

// const dataList = [];
// data.forEach((item) => {
//     if (new Date(item.time).getUTCHours() === 12) {
//         dataList.push(item);
//     }
// });

app.get('/rsi', (req, response) => {
    const baseUrl = 'https://min-api.cryptocompare.com/data';
    request(`${baseUrl}/price?fsym=XLM&tsyms=BTC`, { json: true }, (err, res, priceBody) => {
        if (err) { return console.log(err); }
        request(`${baseUrl}/histohour?aggregate=24&extraParams=CryptoCompare&fsym=XLM&limit=10000&tryConversion=false&tsym=BTC`, { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
            const list = body.Data;
            list.forEach((item) => item.time = +(item.time + '000'));
            list.push({close: priceBody.BTC, time: Date.now()});

            const parsedList = data.map((item) => item.close);
            const close = RSI.calculate({values: parsedList, period: 14});

            const parsedClose =close.map((value, index) => ({close: value, time: list[index + 13].time}));
            return response.send(parsedClose);
        });
    });
});

app.get('/prices', (req, res) => {
    return res.send(data);
});
const baseUrl = 'https://min-api.cryptocompare.com/data';

function isPending(coins, data) {
    let isPending = false;

    coins.forEach((coin) => {
        if (data[coin] && data[coin].pending) {
            isPending = true;
        }
    });

    return isPending;
}

function parseHistoricalClose(data) {
    return data.Data.map((item) => ({close: item.close, time: item.time.toString().length === 10 ? +(item.time + '000'): item.time}));
}

function calculateRSI(list) {
    return RSI.calculate({values: list.map((item) => item.close), period: 14}).map((value, index) => ({value: value, time: list[index + 12].time}))
}

function createRSI(coins, data) {
    coins.forEach((key) => {
        const list = data[key].list;

        data[key].rsi = calculateRSI(list);
        data[key].macd = MACD.calculate({
            values: list.map((item) => item.close),
            fastPeriod        : 12,
            slowPeriod        : 26,
            signalPeriod      : 9,
            SimpleMAOscillator: false,
            SimpleMASignal    : false});

        data[key].macd = data[key].macd.map((value, index) => ({value: value, time: list[index + 25].time}));
    })
}

function createDynamic(coinKey, results, fieldName, values) {
    results[coinKey][fieldName] = values;
    let lastPrice = 0;
    results[coinKey][fieldName].forEach((value, index) => {
        if (index > 0) {
            const result = results[coinKey][fieldName][index] > lastPrice;
            lastPrice = results[coinKey][fieldName][index];
            results[coinKey][fieldName][index] = result;
        } else {
            lastPrice = results[coinKey][fieldName][index];
        }
    })
}

app.put('/coins', (req, response) => {
    const body = req.body;
    const coins = body.symbols;

    const results = {};

    coins.forEach((key) => {
        results[key] = {};
        request(`${baseUrl}/histohour?aggregate=${body.aggregate || 24}&extraParams=CryptoCompare&fsym=${key}&tryConversion=false&tsym=BTC`, {json: true}, (err, res, body) => {

            request(`${baseUrl}/price?fsym=${key}&tsyms=BTC`, {json: true}, (error, priceRes, priceBody) => {
                const lastPrice = {
                    time: Date.now(),
                    close: priceBody.BTC
                };

                body.Data.push(lastPrice);

                results[key].pending = false;
                results[key].list = parseHistoricalClose(body);

                if (!isPending(coins, results)) {
                    createRSI(coins, results);

                    coins.forEach((coinKey) => createDynamic(coinKey, results, 'priceDynamic', results[coinKey].list.map((item) => item.close)));
                    coins.forEach((coinKey) => createDynamic(coinKey, results, 'macdDynamic', results[coinKey].macd.map((item) => item.value.MACD)));

                    return response.send(results);
                }

            });

        });

        results[key].pending = true;
    });

});

app.get('/hisprice', (req, response) => {
    const baseUrl = 'https://min-api.cryptocompare.com/data';
    request(`${baseUrl}/price?fsym=XLM&tsyms=BTC`, { json: true }, (err, res, priceBody) => {
        if (err) { return console.log(err); }
        request(`${baseUrl}/histohour?aggregate=24&extraParams=CryptoCompare&fsym=XLM&limit=10000&tryConversion=false&tsym=BTC`, { json: true }, (err, res, body) => {
            if (err) { return console.log(err); }
            const list = body.Data;
            list.forEach((item) => item.time = +(item.time + '000'));
            list.push({close: priceBody.BTC, time: Date.now()});

            response.send(list);
        });
    });
});

app.get('/macd', (req, res) => {
    const list = data.map((item) => item.close);
    const macdi = res.send(MACD.calculate({
        values: list,
        fastPeriod        : 12,
        slowPeriod        : 26,
        signalPeriod      : 9,
        SimpleMAOscillator: false,
        SimpleMASignal    : false}));

    return res.send(macdi.map((item) => ({})))
});


app.listen(3002, function () {
    console.log('Example app listening on port 3000!');
});